const Fields=document.querySelectorAll('.board__item');

const panel=document.querySelector(".panel");

const button=document.querySelector(".button_reset");

let Field;
let Player;
let gameActive;


const resetGame = () =>
{
    Field=["","","","","","","","",""];
    gameActive= true;
    Player='X';   
}

resetGame();

const winningConditions=[
    [0,1,2],
    [3,4,5],
    [6,7,8],
    [0,3.6],
    [1,4,7],
    [2,4,8],
    [0,4,8],
    [6,4,2]
];

const displayWin= () =>
{
    panel.innerText=`Wygrałeś ${Player}`;
};

const displayWinTie= () =>
{
    panel.innerHTML=`Remis !!`;
};

const clearPanel= () =>
{
     panel.innerText="";
}

const validate= ()=>
{
    let gameWon=false;
    for(let i=0; i<=7; i++){
        
            const [posA,posB,posC]=winningConditions[i];
            const value1=Field[posA];
            const value2=Field[posB];
            const value3=Field[posC];
            
            if(value1 !== "" && value1 === value2 && value1 === value3)
                {
                   gameWon= true;
                    break;
        }
    }
    if(gameWon)
        {
            gameActive=false; 
            displayWin();
            
        }else if(isBoardFull())
                 {
                    gameActive= false;
                     displayWinTie();
                 }
};

const isBoardFull = () =>
{
    return Field.find(Field => Field === '') === undefined;
    
};

Fields.forEach(Fields=>{
    Fields.addEventListener('click', e => {
        const {pos}=e.target.dataset;
       if(gameActive && Field[pos]==="")
        {
   
        Field[pos]=Player;
        e.target.classList.add(`board__item--filled-${Player}`);
        validate();
        Player=Player=== 'X' ? 'O' : 'X';
        }
    });
});

const reserBoard = () =>
{
   Fields.forEach(Field => {
        Field.classList.remove('board__item--filled-X','board__item--filled-O');
    }); 
}

const handleButtonClick = () =>
{
    resetGame();
    reserBoard();
    clearPanel();
};


button.addEventListener('click',handleButtonClick)






    

